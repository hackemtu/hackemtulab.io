$(document).ready(function () {
    $('.dt_submit').on('click', function (event) {
        event.preventDefault();
        var name_attr = [];
        var values = [];
        var dt_process = "";
        if($(this).closest("section").attr('id') !== undefined)
        {
            var section_id = $(this).closest("section").attr('id');
        }else{
            var section_id = $(this).closest("footer").attr('id');
        }
        var submit_loader = '<div class="loading color-dark-green display-inline-block margin-left-15 vertical-align-top no-margin-right margin-top-10" id="loading">Loading...</div>';
        $('#' + section_id).find('form').find('button').after(submit_loader);
        $('#' + section_id).find('form input, form select,form textarea').each(
                function (index) {
                    
                    if ($(this).is('[data-email="required"]')) {
                        var required_val = $(this).val();
                        if (required_val != '') {
                            name_attr.push($(this).attr('name'));
                            values.push($(this).val());
                            dt_process = true;
                        } else {
                            $('#loading').remove();
                            $(this).addClass('dt_input_error');
                            dt_process = false;
                        }
                    }

                    if (!$(this).is('[data-email="required"]')) {
                        name_attr.push($(this).attr('name'));
                        values.push($(this).val());
                    }

                });
        
        var captcha_length = $('.g-recaptcha').length;
        if (captcha_length >= 1) {
            var response = grecaptcha.getResponse();
            //recaptcha failed validation
            if (response.length == 0) {
                $('#loading').remove();
                $('#google-recaptcha-error').remove();
                $('#' + section_id).find('.g-recaptcha').after('<span class="google-recaptcha-error" id="google-recaptcha-error">Invalid recaptcha</span>');
                dt_process = false;
            } else {
                $('#google-recaptcha-error').remove();
                $('#recaptcha-error').hide();
                dt_process = true;
            }
        }
        if (dt_process) 
        {
            localStorage.setItem('dt_section',section_id);
            $.post("onepage_mail/contact.php", {
                data: { input_name: name_attr,values:values,section_id:section_id},
                type: "POST",
            }, function (data) {
                $('#loading').remove();
                var dt_form_output = '';
                if(data) 
                {
                    if(data.type == "dt_message") 
                    {
                       $('#error').remove(); 
                       $('#success').remove();
                       $('#google-recaptcha-error').remove(); 
                       var dt_form_output = '<div id="success" class="no-margin-lr alt-font text-align-center color-white bg-dark-green padding-all-10 margin-bottom-30">'+data.text+'</div>';
                    }else if (data.type == "dt_error") {
                        $('#success').remove();
                        $('#error').remove(); 
                        var dt_form_output = '<div id="error" class="no-margin-lr alt-font text-align-center color-white bg-cream-pink padding-all-10 margin-bottom-30">'+data.text+'</div>';
                    }else{
                        var dt_form_output = '';
                    } 
                }

                if(dt_form_output != '')
                {
                    var section_id = localStorage.getItem('dt_section');
                    $('#'+section_id).find('form').before(dt_form_output);
                }
                $('#' + section_id).find('form input,form textarea').each(function (index) {
                    $(this).val('');
                    $(this).removeClass('dt_input_error');
                });

                setTimeout(function(){
                    $('#success').fadeOut();
                    $('#success').remove();
                    $('#error').fadeOut();
                    $('#error').remove();
                    $(this).submit();
                 },5000);
                localStorage.removeItem('dt_section');
            }, 'json');
        }
        
        $('#' + section_id).find('form input,form textarea').each(function (index) {
            $(this).keypress(function () {
                $(this).removeClass('dt_input_error');
            });
        });

        $('#' + section_id).find('form input,form textarea').each(function (index) {
            if ($(this).is(":focus")) {
                $(this).removeClass('dt_input_error');
            }
        });

        $('#' + section_id).find('form select').each(function (index) {
            $(this).on("change", function () {
                var val = this.value;
                if (val == ''){
                    $(this).removeClass('dt_input_error');
                }
            });
        });
    });
    
});